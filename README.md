# Trains

A simple java console program to play with [trains](https://bitbucket.org/Maringele/trains).

## Prerequisites

This little program was build and tested with

- Apache Maven 3.5.4 
- java 10.0.1 2018-04-17
- junit 4.12

## Build, test and run

(will download a lot of stuff at first run)

```
$ git clone https://bitbucket.org/Maringele/trains.git
$ cd trains
$ mvn clean package
...
Tests run: 17, Failures: 0, Errors: 0, Skipped: 0
...
$ mvn exec:java

Trains Sample

number of vehicles: 4, total length: 182,0 m:-
...
```

