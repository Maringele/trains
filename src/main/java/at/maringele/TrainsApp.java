package at.maringele;

import at.maringele.trains.*;
import at.maringele.trains.ILocomotive.EngineType;
import at.maringele.trains.IWagon.WagonType;

/**
 * Hello world!
 *
 */
public class TrainsApp 
{
    public static void main( String[] args )
    {
        System.out.println( "\nTrains Sample");

        var manufacturer = Manufacturer.getManufacturer("Big Corp");

        // create vehicles
        var locomotive = manufacturer.buildLocomotive(EngineType.Diesel, 500*1000, 10*000, 60);
        var wagon = manufacturer.buildWagon(WagonType.Passenger, 5*1000, 200);
        var freight = manufacturer.buildWagon(WagonType.Fright, 50*1000, 0);
        var mixed = manufacturer.buildWagon(WagonType.Dining, 10*1000, 150);

        wagon.lock(locomotive);
        wagon.couple(freight);

        locomotive.append(mixed);

        System.out.format("\nnumber of vehicles: %d, total length: %.1f m:\n", locomotive.numberOfVehicles(),locomotive.totalLength());
        System.out.format("%s\n\n", locomotive.toString());

        freight.unlock();

        System.out.format("number of vehicles: %d, total length: %.1f m:\n", locomotive.numberOfVehicles(),locomotive.totalLength());
        System.out.format("%s\n\n", locomotive.toString());







    }
}
