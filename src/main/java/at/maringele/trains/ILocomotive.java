package at.maringele.trains;

/** 
 * - A locomotive is a vehicle with load and passengers
 * - A single locomotive is allready a train. 
 * */
public interface ILocomotive extends IVehicle, ITrain {
    public enum EngineType {
        Steam, Diesel, Electric
    }

    /** get the engine type of a locomotive */
    EngineType getEngineType();

    /** get additonal traction force in kg, i.e. which weight 
     * additonal to its own dead weight can be pulled. 
     * If this is negative the locomotive cannot pull itself.
     */
    int getAdditonalTraction();

    /** calculate traction, this should be bigger than the dead weight */
    default int getTraction() {
        return getDeadWeight() + getAdditonalTraction();
    }

    // IVehicle default implementations

    default int getMaxDrag() {
        return getMaxCombinedWeight() - getTraction();
    }

    // ITrain default implementations

    default boolean isReadyToStart() {
        return totalMaxDrag() >= 0;
    }

    default boolean append(IVehicle vehicle) {
        return this.tail().couple(vehicle);
    }
}