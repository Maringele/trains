package at.maringele.trains;

interface ITrain {
    boolean isReadyToStart();

    boolean append(IVehicle vehicle);

    int numberOfVehicles();
}