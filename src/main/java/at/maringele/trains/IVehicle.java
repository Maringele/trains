package at.maringele.trains;

import at.maringele.utils.IStreamable;

interface IVehicle extends IStreamable<IVehicle> {
    // enum VehicleType {
    //     SteamLocomotive, DieselLocomotive, ElectricLocomotive,
    //     ChairWagon, DiningWagon, SleepingWagon, 
    //     OpenWagon, CoveredWagon, Refrigerated, BaggageWagon,å
    //     FlatWagon, TankWagon, SpineWagon, MineralWagon,
    //     CarsWagon, TrucksWagon
    // }

    /** get length of vehicle in m */
    double getLength();

    /** get dead weight of vehicle in kg */
    int getDeadWeight();

    /**  get maximal load capacity in kg */
    int getMaxLoadWeight();

    /** get maximal number of passengers */
    int getMaxNumberOfPassengers();

    /** get manufactorer */
    String getManufacturer();

    /** get build year */
    int getBuildYear();

    /** get serial number (unique per manufacturer) */
    int getSerialNumber();

    /** */
    int getMaxDrag();

    IVehicle getPrevVehicle();
    IVehicle getNextVehicle();

    /** 
     * Couple this vehicle to other vehicle, 
     * then lock other vehicle to this vehicle.
     * */
    boolean couple(IVehicle other);

    /** 
     * If other vehicle is allready coupled to this vehicle,
     * then lock this vehicle to other vehicle.
     * */
    boolean lock(IVehicle other);

    /** 
     * Unlock this vehicle from previous vehicle 
     * then uncouple previous vehicle from this vehicle 
     * */
    void unlock();

    /** 
     * If next vehicle is allready unlocked from this vehicles
     * then uncouple this vehicle from next vehicle.
     * */
    void decouple();

    default String getClassificaton() {
        return String.format("%.1f m | %d kg | %d P", getLength(), getMaxLoadWeight(), getMaxNumberOfPassengers());
    }

    /** find the head, i.e. the first of connected vehicles, starting from this vehicle */
    default IVehicle head() {
        if (this.getPrevVehicle() == null) { return this; }

        assert this.getPrevVehicle().getNextVehicle() == this : "fail fast: corrupted double linked list";
        return this.getPrevVehicle().head();
    }

    /** find the tail, i.e. the last of connected vehicles, starting from this vehicle */
    default IVehicle tail() {
        if (this.getNextVehicle() == null) { return this; }

        assert this.getNextVehicle().getPrevVehicle() == this : "fail fast: corrupted double linked list";
        
        return this.getNextVehicle().tail();
    }

    /** check if this or one of its successors equals other */
    default boolean hasFollowUp(IVehicle other) {
        for (IVehicle vehicle : this) {
            if (vehicle == other) { return true; }
        }
        return false;
    }

    /** checks if this vehicle is locked to a predecessor */
    default boolean isLocked() {
        return getPrevVehicle() != null;
    }

    /** checks if this vehicle is coupled to a successor */
    default boolean isCoupled() {
        return getNextVehicle() != null;
    }

    default boolean isCoupledAndLockedWith(IVehicle other) {
        return this.getNextVehicle() == other && this == other.getPrevVehicle();
    }

    default int getMaxCombinedWeight() {
        return getDeadWeight() + getMaxLoadWeight() + getMaxNumberOfPassengers() * 75;
    }


    /** the sum of the length of all connected vehicle together togeter */
    default double totalLength() {
        return head().stream().reduce(
            0.0, 
            (x,y) -> x + y.getLength(), 
            (x,y) -> x + y);
    }

    default int totalMaxNumberOfPassengers() {
        return head().stream().reduce(
            0, 
            (x,y) -> x + y.getMaxNumberOfPassengers(), 
            (x,y) -> x + y);
    }

    default int requiredConductors() {
        var count = totalMaxNumberOfPassengers();
        return count / 50 + ((count % 50) > 0 ? 1 : 0);
    }

    default int totalDeadWeight() {
        return head().stream().reduce(
            0, 
            (x,y) -> x + y.getDeadWeight(), 
            (x,y) -> x + y);
    }

    default int totalMaxLoadWeight() {
        return head().stream().reduce(
            0, 
            (x,y) -> x + y.getMaxLoadWeight(), 
            (x,y) -> x + y);
    }

    default int totalMaxCombinedWeight() {
        return head().stream().reduce(
            0, 
            (x,y) -> x + y.getMaxCombinedWeight(), 
            (x,y) -> x + y);
    }

    default int totalMaxDrag() {
        return head().stream().reduce(
            0, 
            (x,y) -> x + y.getMaxDrag(), 
            (x,y) -> x + y);
    }













    
}