package at.maringele.trains;

public interface IWagon extends IVehicle {
    public enum WagonType {
        Passenger, Couchette, Sleeping, Dining, 
        OpenWagon, CoveredWagon, Refrigerated, Fright,
        FlatWagons, TankWagon, SpineWagon, MineralWagon
    }

    WagonType getWagonType();

    default int getMaxDrag() {
        return getMaxCombinedWeight();
    }

 

    
}