package at.maringele.trains;

class Locomotive extends Vehicle implements ILocomotive {
    private EngineType engineType;
    private int additonalTraction;

    /**
     * @return the engine type of the locomotive
     */
    public EngineType getEngineType() {
        return engineType;
    }

    /**
     * @return the the addional traction, i.e. the additional weith the locomotive can pull
     */
    public int getAdditonalTraction() {
        return additonalTraction;
    }

    @Override
    public String getClassificaton() {
        return  String.format("%s-%s : %s", engineType.toString(), getClass().getSimpleName(), super.getClassificaton());
    }

    Locomotive
    (
        EngineType engineType,
        int additonalTraction,
        double length,
        int deadWeight,
        int maxLoadWeight,
        int maxNumberOfPassengers,
        String manufacturer,
        int buildYear,
        int serialNumber
    ) {
        this.engineType = engineType;
        this.additonalTraction = additonalTraction;
        this.length = length;
        this.deadWeight = deadWeight;
        this.maxLoadWeight = maxLoadWeight;
        this.maxNumberOfPassengers = maxNumberOfPassengers;
        this.manufacturer = manufacturer;
        this.buildYear = buildYear;
        this.serialNumber = serialNumber;
    }

}