package at.maringele.trains;

import java.util.HashMap;
import at.maringele.utils.Pair;

// import at.maringele.trains.
// import at.maringele.trains.IVehicle.VehicleType;
// import at.maringele.utils.Pair;
import java.util.List;

/** A manufacturer produces locomotives and wagons */
public class Manufacturer {

    private static HashMap<String, Manufacturer> manufacturers = new HashMap<String,Manufacturer>();

    private int serialNumber = 0;
    private String name;

    private Pair<Double, Integer> specifyLengthAndDeadWeight(
        ILocomotive.EngineType engineType, 
        int fractionForce, 
        int maxLoadWeight, 
        int maxNumberOfPassengers) {
            // passengers, load, engine increase length and deadweight 
            double length = 2 + maxNumberOfPassengers / 4 + maxLoadWeight/1000;
            int deadWeight = maxNumberOfPassengers * 20 + maxLoadWeight / 4 + (int)(length / 4.0);
            switch (engineType) {
                case Electric: length += 5; deadWeight += 500; break;
                case Diesel: length += 7; deadWeight += 700; break;
                case Steam: length += 10; deadWeight += 1000; break;
            }
            return new Pair<Double, Integer>(length, deadWeight);
    }
    
    private Pair<Double, Integer>  specifyLengthAndDeadWeight(
        IWagon.WagonType wagonType, 
        int maxLoadWeight, 
        int maxNumberOfPassengers) {
             // passengers, load, engine increase length and deadweight 
            double length = 2 + maxNumberOfPassengers / 4 + maxLoadWeight/1000;
            int deadWeight = maxNumberOfPassengers * 20 + maxLoadWeight / 4 + (int)(length / 4.0);
             return new Pair<Double, Integer>(length, deadWeight);
    }

    private Manufacturer(String name) {
        this.name = name;
    }

    static public Manufacturer getManufacturer(String name) {
        Manufacturer m = manufacturers.get(name);
        if (m == null) {
            m = new Manufacturer(name);
            manufacturers.put(name, m);
        }
        return m;
    }

    // IVehicle buildVehicle(Pair<VehicleType, Integer> specification) {
    //     return null;
    // }

    public IWagon buildWagon(
            IWagon.WagonType wagonType,
            int maxLoadWeight,
            int maxNumberOfPassengers) {
                // a vehicle with clearly invalid specification must not be created
                if (maxLoadWeight < 0 || maxNumberOfPassengers < 0) {
                    return null;
                }

                var lengthWeightPair = specifyLengthAndDeadWeight(wagonType, maxLoadWeight, maxNumberOfPassengers);

                // use a fresh serial number
                this.serialNumber += 1;

                return new
                Wagon(
                    wagonType,
                    lengthWeightPair.getLhs(), 
                    lengthWeightPair.getRhs(), 
                    maxLoadWeight, 
                    maxNumberOfPassengers, 
                    this.name, 
                    "WAGON",
                    java.time.Year.now().getValue(), 
                    this.serialNumber);
            }

        

    
    public ILocomotive buildLocomotive(
        ILocomotive.EngineType engineType,
        int additonalTraction,
        int maxLoadWeight,
        int maxNumberOfPassengers) {
            // a locomotive with clearly invalid specification must not be created
            if (maxLoadWeight < 0 || maxNumberOfPassengers < 0 || additonalTraction < 0) {
                return null;
            }

            var lengthWeightPair = specifyLengthAndDeadWeight(engineType, additonalTraction, maxLoadWeight, maxNumberOfPassengers);

            this.serialNumber += 1;

            return new Locomotive(
                engineType,
                additonalTraction,
                lengthWeightPair.getLhs(), 
                lengthWeightPair.getRhs(), 
                maxLoadWeight, 
                maxNumberOfPassengers, 
                this.name, 
                java.time.Year.now().getValue(), 
                this.serialNumber
            );

        }

}