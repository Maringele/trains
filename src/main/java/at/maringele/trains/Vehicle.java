package at.maringele.trains;

import java.util.Iterator;

abstract class Vehicle implements IVehicle {
    protected double length;
    protected int deadWeight;
    protected int maxLoadWeight;
    protected int maxNumberOfPassengers;
    protected String type;
    protected String manufacturer;
    protected int buildYear;
    protected int serialNumber;

    protected IVehicle nextVehicle = null;
    protected IVehicle prevVehicle = null;

    @Override
    public double getLength() {
        return length;
    }

    /**
     * @return the deadWeight
     */
    public int getDeadWeight() {
        return deadWeight;
    }

    /**
     * @return the maxLoadWeight
     */
    public int getMaxLoadWeight() {
        return maxLoadWeight;
    }

    /**
     * @return the maxNumberOfPassengers
     */
    public int getMaxNumberOfPassengers() {
        return maxNumberOfPassengers;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @return the manufacturer
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * @return the buildYear
     */
    public int getBuildYear() {
        return buildYear;
    }

    /**
     * @return the serialNumber
     */
    public int getSerialNumber() {
        return serialNumber;
    }

    /**
     * @return the prevVehicle
     */
    public IVehicle getNextVehicle() {
        return nextVehicle;
    }

    /**
     * @return the prevVehicle
     */
    public IVehicle getPrevVehicle() {
        return prevVehicle;
    }

    public int numberOfVehicles() {
        return (int)head().stream().count();
    }

    @Override
    public String toString() {
        return head().stream().reduce(
            "",
            (x,y) -> x.isEmpty() ? String.format("-[ %s ]-", y.getClassificaton()):  String.format("%sx-[ %s ]-", x, y.getClassificaton()),
            (x,y) -> String.format("%s%s", x, y)
        );

    }

    /**
     * - couples this to other vehicle
     * - locks other to this vehicle
     */
    public boolean couple(IVehicle other) {
        if (other == null || other == this) { return false; }

        // this<->other
        if (this.isCoupledAndLockedWith(other)) { return true; }

        // other is allready locked to a different vehicle than this.
        // X<- other, X != this
        if (other.isLocked()) { return false; }

        // other has successor this
        // other -> ... ->this
        if (other.hasFollowUp(this)) { return false; }

        // this is allready coupled to a different vehicle than other.
        // this ->Y, Y != other
        if (this.isCoupled()) { return false; }

        // couple this vehicle to next vehicle
        this.nextVehicle = other;

        // lock next vehicle to this vehicle
        other.lock(this);

        return true;
    }

    /** 
     * - couple other to this vehicle
     * - lock this to other vehicle
     * other<->this
     */
    public boolean lock(IVehicle other) {
        // other<->this
        if (other.isCoupledAndLockedWith(this)) { return true; }

        if (other.getNextVehicle() == this) { 
            // other vehicle is allready coupled to this vehicle
            this.prevVehicle = other;
            return true;
        }
        else {
            return other.couple(this);
        }
    }

    /** 
     * prev<->this
     * - unlocks this from prev vehicle
     * - uncouples prve from this vehicle
     */
    public void unlock() {
        // check if this vehicle is unlocked
        if (this.isLocked()) { 
            var prev = prevVehicle; // i.e. unlock
            prevVehicle = null;
            prev.decouple();
        }
    }

    /** 
     * this<->next
     * - unlocks next from this vehicle
     * - decouples this from next vehicle
     */
    public void decouple() {
        // check if this vehicle is coupled
        if (this.isCoupled()) {
            // check if next vehicle is still locked (to this vehicle)
            if (nextVehicle.isLocked()) {
                nextVehicle.unlock();
            }
            else {
                nextVehicle = null;
            }
        }
    }



    public boolean hasSuccessor(IVehicle vehicle) {
        if (vehicle == null) { return true; }
        IVehicle lhs = this;
        while (lhs != null) {
            if (lhs == vehicle) { return true; }
            lhs = lhs.getNextVehicle();
        }


        return false;
    }

    class VehicleIterator implements Iterator<IVehicle> {
        private IVehicle nextVehicle;
        VehicleIterator(IVehicle vehicle) {
            this.nextVehicle = vehicle;
        }

        public boolean hasNext() {
            return nextVehicle != null;
        }

        public IVehicle next() {
            var vehicle = nextVehicle;
            nextVehicle = vehicle.getNextVehicle();
            return vehicle;
        }

    }

    public Iterator<IVehicle> iterator() {
        return new VehicleIterator(this);
    }









}