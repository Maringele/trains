package at.maringele.trains;

final class Wagon extends Vehicle implements IWagon {
    private IWagon.WagonType wagonType;
    
    /**
     * @return the wagonType
     */
    public IWagon.WagonType getWagonType() {
        return wagonType;
    }

    @Override
    public String getClassificaton() {
        return  String.format("%s-%s : %s", wagonType.toString(), getClass().getSimpleName(), super.getClassificaton());
    }

    Wagon(
        WagonType wagonType,
        double length,
        int deadWeight,
        int maxLoadWeight,
        int maxNumberOfPassengers,
        String manufacturer,
        String type,
        int buildYear,
        int serialNumber
    ) {
        this.wagonType = wagonType;
        this.length = length;
        this.deadWeight = deadWeight;
        this.maxLoadWeight = maxLoadWeight;
        this.maxNumberOfPassengers = maxNumberOfPassengers;
        this.manufacturer = manufacturer;
        this.type = type;
        this.buildYear = buildYear;
        this.serialNumber = serialNumber;
    }

}