package at.maringele.utils;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface IStreamable<T> extends Iterable<T> {
    default Stream<T> stream() {
        return StreamSupport.stream(spliterator(), false);
    }
}