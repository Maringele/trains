package at.maringele.utils;

/** Immutable pair of two values */
public class Pair<L,R> {
    private L lhs;
    private R rhs;

    /** get left value of the pair */
    public L getLhs() { return lhs; }
    
    /** get right value of the pair */
    public R getRhs() { return rhs; } 

    /** create pair of two values */
    public Pair(L lhs, R rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public int hashCode() {
        return getLhs().hashCode() * getRhs().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        var other = obj instanceof Pair<?,?> ? (Pair<?,?>) obj : null;

        return other != null && this.getLhs().equals(other.getLhs()) && this.getRhs().equals(other.getRhs());
    }
}

