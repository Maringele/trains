package at.maringele.trains;

import static org.junit.Assert.*;
import org.junit.Test;
import java.util.List;
import java.util.Arrays;
import java.util.stream.Collectors;

import at.maringele.trains.ILocomotive.EngineType;
import at.maringele.trains.IWagon.WagonType;

public class LocomotiveTest {
    Manufacturer manufacturer = Manufacturer.getManufacturer("Big Corp");
    
    @Test
    public void testLocomotives() {
        var additonalFraction = 200 * 1000;
        var maxLoadWeight = 50 * 1000;
        var maxNumberOfPassengers = 20;

        List<EngineType> engines = Arrays.asList(EngineType.Steam, EngineType.Diesel, EngineType.Electric);
        List<ILocomotive> locomotives = engines.stream().map(
            type -> manufacturer.buildLocomotive(type, additonalFraction, maxLoadWeight, maxNumberOfPassengers)
        ).collect(Collectors.toList());

        for (ILocomotive locomotive : locomotives ) {
            assertEquals(additonalFraction, locomotive.getAdditonalTraction());
            assertEquals(maxLoadWeight, locomotive.getMaxLoadWeight());
            assertEquals(maxNumberOfPassengers, locomotive.getMaxNumberOfPassengers());

            assertTrue(locomotive.getMaxDrag() < 0);

        }
    }

}