package at.maringele.trains;

import static org.junit.Assert.*;
import org.junit.Test;

import at.maringele.trains.ILocomotive.EngineType;
import at.maringele.trains.IWagon.WagonType;

public class ManufacturerTest {
    @Test
    public void testGetManufacturer() {
        var m1 = Manufacturer.getManufacturer("Big Corp");
        var m2 = Manufacturer.getManufacturer("Small Fry");

        var m3 = Manufacturer.getManufacturer("Big Corp");

        assertTrue(m1 == m3);
        assertFalse(m1 == m2);
        assertFalse(m2 == m3);
    }

    @Test
    public void testCreateWagon() {
        var m1 = Manufacturer.getManufacturer("Big fish");

        var w1 = m1.buildWagon(WagonType.Couchette, 2*1000, 82);
        var w2 = m1.buildWagon(WagonType.Fright, 30*1000, 12);
        
        assertTrue(w1 != null);
        assertTrue(w2 != null);
        assertFalse(w1.equals(w2));
        // assertTrue(w1.getLength() == w2.getLength());
        // assertTrue(w1.getDeadWeight() == w2.getDeadWeight());
        // assertTrue(w1.getMaxLoadWeight() == w2.getMaxLoadWeight());
        // assertTrue(w1.getMaxNumberOfPassengers() == w2.getMaxNumberOfPassengers());      
        // assertFalse(w1.getClassificaton() == w2.getClassificaton());

        System.out.println(w1.getClassificaton());
        assertTrue(w1.getManufacturer() == w2.getManufacturer());
        assertTrue(w1.getBuildYear() == w2.getBuildYear());  
        assertFalse(w1.getSerialNumber() == w2.getSerialNumber());
    }

    @Test
    public void testCreateLocomotive() {
        var m1 = Manufacturer.getManufacturer("Big fish");

        var maxLoad = 25*1000;
        var additonalFraction = maxLoad*30; // pull 30 vehicles
        var maxPassengers = 20;

        var diesel = m1.buildLocomotive(EngineType.Diesel, additonalFraction, maxLoad, maxPassengers);
        var steam = m1.buildLocomotive(EngineType.Steam, additonalFraction, maxLoad, maxPassengers);
        
        assertTrue(diesel != null);
        assertTrue(steam != null);
        assertFalse(diesel == steam);
        assertFalse(diesel.equals(steam));
        assertTrue(diesel.getLength() < steam.getLength());

        assertTrue(diesel.getDeadWeight() < steam.getDeadWeight());
        assertTrue(diesel.getMaxLoadWeight() == steam.getMaxLoadWeight());
        assertTrue(diesel.getMaxNumberOfPassengers() == steam.getMaxNumberOfPassengers());      
        assertFalse(diesel.getClassificaton() == steam.getClassificaton());
    }

}

