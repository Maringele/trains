package at.maringele.trains;

import static org.junit.Assert.*;
import org.junit.Test;

import at.maringele.trains.ILocomotive.EngineType;
import at.maringele.trains.IWagon.WagonType;

public class VehicleTest {
    private Manufacturer manufacturer = Manufacturer.getManufacturer("Big Corp");

    @Test
    public void testCouple() {
        // create vehicles
        var locomotive = manufacturer.buildLocomotive(EngineType.Diesel, 50*1000, 10000, 60);
        var wagon = manufacturer.buildWagon(WagonType.Passenger, 5*1000, 200);
        var freight = manufacturer.buildWagon(WagonType.Fright, 500*1000, 0);

        // coupling to itself must fail
        assertFalse(locomotive.couple(locomotive));
        assertFalse(wagon.couple(wagon));
        assertFalse(freight.couple(freight));

        // coupling must succeed
        assertTrue(locomotive.couple(wagon));
        assertTrue(wagon.couple(freight));

        // check if locomotive<->wagon<->freight
        assertEquals(null, locomotive.getPrevVehicle());
        assertEquals(wagon, locomotive.getNextVehicle());

        assertEquals(locomotive, wagon.getPrevVehicle());
        assertEquals(freight, wagon.getNextVehicle());

        assertEquals(wagon, freight.getPrevVehicle());
        assertEquals(null, freight.getNextVehicle());

        assertEquals(3, locomotive.numberOfVehicles());

        // now coupling must fail for uncoupled vehicles
        // but succeed for allready coupled vehicles

        assertFalse(locomotive.couple(locomotive));
        assertTrue(locomotive.couple(wagon));       // locomotive<->wagon
        assertFalse(locomotive.couple(freight));

        assertFalse(wagon.couple(locomotive));
        assertFalse(wagon.couple(wagon));
        assertTrue(wagon.couple(freight));          // wagon<->freight

        assertFalse(freight.couple(locomotive));
        assertFalse(freight.couple(wagon));
        assertFalse(freight.couple(freight));
    }@Test
    public void testLock() {
        // create vehicles
        var locomotive = manufacturer.buildLocomotive(EngineType.Diesel, 50*1000, 10000, 60);
        var wagon = manufacturer.buildWagon(WagonType.Passenger, 5*1000, 200);
        var freight = manufacturer.buildWagon(WagonType.Fright, 500*1000, 0);

        // lock to itself must fail
        assertFalse(locomotive.lock(locomotive));
        assertFalse(wagon.lock(wagon));
        assertFalse(freight.lock(freight));

        // coupling must succeed
        assertTrue(wagon.lock(locomotive));
        assertTrue(freight.lock(wagon));

        // check if locomotive<->wagon<->freight
        assertEquals(null, locomotive.getPrevVehicle());
        assertEquals(wagon, locomotive.getNextVehicle());

        assertEquals(locomotive, wagon.getPrevVehicle());
        assertEquals(freight, wagon.getNextVehicle());

        assertEquals(wagon, freight.getPrevVehicle());
        assertEquals(null, freight.getNextVehicle());

        // now coupling must fail for uncoupled vehicles
        // but succeed for allready coupled vehicles

        assertFalse(locomotive.lock(locomotive));
        assertTrue(wagon.lock(locomotive));       // locomotive<->wagon
        assertFalse(freight.lock(locomotive));

        assertFalse(locomotive.lock(wagon));
        assertFalse(wagon.lock(wagon));
        assertTrue(freight.lock(wagon));          // wagon<->freight

        assertFalse(locomotive.lock(freight));
        assertFalse(freight.lock(freight));
        assertFalse(wagon.lock(freight));
    }



    @Test
    public void testUnlockAndDecouple() {
        // create vehicles
        var locomotive = manufacturer.buildLocomotive(EngineType.Diesel, 50*1000, 10000, 60);
        var wagon = manufacturer.buildWagon(WagonType.Passenger, 5*1000, 200);
        var freight = manufacturer.buildWagon(WagonType.Fright, 500*1000, 0);

        // couple vehicles
        locomotive.couple(wagon);
        wagon.couple(freight);

        wagon.unlock();
        wagon.decouple();

        assertEquals(null, locomotive.getPrevVehicle());
        assertEquals(null, locomotive.getNextVehicle());

        assertEquals(null, wagon.getPrevVehicle());
        assertEquals(null, wagon.getNextVehicle());

        assertEquals(null, freight.getPrevVehicle());
        assertEquals(null, freight.getNextVehicle());
    }

    @Test
    public void testFirst() {
        // create vehicles
        var locomotive = manufacturer.buildLocomotive(EngineType.Diesel, 50*1000, 10000, 60);
        var wagon = manufacturer.buildWagon(WagonType.Passenger, 5*1000, 200);
        var freight = manufacturer.buildWagon(WagonType.Fright, 500*1000, 0);

        // couple vehicles
        locomotive.couple(wagon);
        wagon.couple(freight);
        freight.couple(locomotive);

        assertEquals(locomotive, locomotive.head());
        assertEquals(locomotive, wagon.head());
        assertEquals(locomotive, freight.head());
    }
    
    @Test
    public void testLast() {
        // create vehicles
        var locomotive = manufacturer.buildLocomotive(EngineType.Diesel, 50*1000, 10000, 60);
        var wagon = manufacturer.buildWagon(WagonType.Passenger, 5*1000, 200);
        var freight = manufacturer.buildWagon(WagonType.Fright, 500*1000, 0);

        // couple vehicles
        locomotive.couple(wagon);
        wagon.couple(freight);
        freight.couple(locomotive);

        assertEquals(freight, locomotive.tail());
        assertEquals(freight, wagon.tail());
        assertEquals(freight, freight.tail());
    }

    @Test    public void testContains() {
        // create vehicles
        var locomotive = manufacturer.buildLocomotive(EngineType.Diesel, 50*1000, 10000, 60);
        var wagon = manufacturer.buildWagon(WagonType.Passenger, 5*1000, 200);
        var freight = manufacturer.buildWagon(WagonType.Fright, 500*1000, 0);

        // couple vehicles
        locomotive.couple(wagon);
        wagon.couple(freight);
        freight.couple(locomotive);

        assertTrue(locomotive.hasFollowUp(locomotive));
        assertTrue(locomotive.hasFollowUp(wagon));
        assertTrue(locomotive.hasFollowUp(freight));

        assertFalse(wagon.hasFollowUp(locomotive));
        assertTrue(wagon.hasFollowUp(wagon));
        assertTrue(wagon.hasFollowUp(freight));

        assertFalse(freight.hasFollowUp(locomotive));
        assertFalse(freight.hasFollowUp(wagon));
        assertTrue(freight.hasFollowUp(freight));

    }

    @Test
    public void testTotalLength() {
        // create vehicles
        var locomotive = manufacturer.buildLocomotive(EngineType.Diesel, 50*1000, 10000, 60);
        var wagon = manufacturer.buildWagon(WagonType.Passenger, 5*1000, 200);
        var freight = manufacturer.buildWagon(WagonType.Fright, 500*1000, 0);

        // couple vehicles
        locomotive.couple(wagon);
        wagon.couple(freight);
        freight.couple(locomotive);

        var expected = locomotive.getLength() + wagon.getLength() + freight.getLength();

        assertEquals(expected, locomotive.totalLength(),0.01);
        assertEquals(expected, wagon.totalLength(),0.01);
        assertEquals(expected, wagon.totalLength(),0.01);
    }

    @Test
    public void testGetMaxCombinedWeight() {
        // create vehicles
        var locomotive = manufacturer.buildLocomotive(EngineType.Diesel, 50*1000, 10000, 60);
        var wagon = manufacturer.buildWagon(WagonType.Passenger, 5*1000, 200);

        var expected = locomotive.getDeadWeight() + locomotive.getMaxLoadWeight() + locomotive.getMaxNumberOfPassengers() * 75;

        assertEquals(expected, locomotive.getMaxCombinedWeight());

        expected = wagon.getDeadWeight() + wagon.getMaxLoadWeight() + wagon.getMaxNumberOfPassengers() * 75;
        assertEquals(expected, wagon.getMaxCombinedWeight());
    }

    @Test
    public void testTotalMaxCombinedWeight() {
        // create vehicles
        var locomotive = manufacturer.buildLocomotive(EngineType.Diesel, 50*1000, 10000, 60);
        var wagon = manufacturer.buildWagon(WagonType.Passenger, 5*1000, 200);
        var freight = manufacturer.buildWagon(WagonType.Fright, 500*1000, 0);

        // couple vehicles
        locomotive.couple(wagon);
        wagon.couple(freight);
        freight.couple(locomotive);

        var expected = locomotive.getMaxCombinedWeight() + wagon.getMaxCombinedWeight() + freight.getMaxCombinedWeight();

        assertEquals(expected, locomotive.totalMaxCombinedWeight());
        assertEquals(expected, wagon.totalMaxCombinedWeight());
        assertEquals(expected, wagon.totalMaxCombinedWeight());
    }

    @Test
    public void testGetMaxDrag() {
        // create vehicles
        var locomotive = manufacturer.buildLocomotive(EngineType.Diesel, 50*1000, 10000, 60);
        var wagon = manufacturer.buildWagon(WagonType.Passenger, 5*1000, 200);

        var expected = locomotive.getMaxCombinedWeight() - locomotive.getTraction();
        assertEquals(expected, locomotive.totalMaxDrag());

        assertEquals(wagon.getMaxCombinedWeight(), wagon.getMaxDrag());
    }

    @Test
    public void testTotalMaxDrag() {
        // create vehicles
        var locomotive = manufacturer.buildLocomotive(EngineType.Diesel, 50*1000, 10000, 60);
        var wagon = manufacturer.buildWagon(WagonType.Passenger, 5*1000, 200);
        var freight = manufacturer.buildWagon(WagonType.Fright, 500*1000, 0);

        // couple vehicles
        locomotive.couple(wagon);
        wagon.couple(freight);
        freight.couple(locomotive);

        var expected = locomotive.getMaxDrag() + wagon.getMaxDrag() + freight.getMaxDrag();

        assertEquals(expected, locomotive.totalMaxDrag());
        assertEquals(expected, wagon.totalMaxDrag());
        assertEquals(expected, wagon.totalMaxDrag());
    }
}