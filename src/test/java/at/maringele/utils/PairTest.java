package at.maringele.utils;

import static org.junit.Assert.*;
import org.junit.Test;

public class PairTest {
    @Test
    public void testPair() {
        var id1 = new Pair<Integer, Double>(4, 5.0);
        var di1 = new Pair<Double, Integer>(4.0, 5);
        var id2 = new Pair<Integer, Double>(4, 5.0);
        var id3 = new Pair<Integer, Double>(3, 5.0);

        assertFalse(id1.equals(di1));
        assertFalse(id1.hashCode() == di1.hashCode());

        assertTrue(id1.equals(id2));
        assertTrue(id1.hashCode() == id2.hashCode());
        assertFalse(id1 == id2);

        assertFalse(id1.equals(id3));
        assertFalse(id1.hashCode() == id3.hashCode());
        assertFalse(id1 == id3);
    }


}